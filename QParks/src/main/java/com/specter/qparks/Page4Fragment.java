package com.specter.qparks;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;


/**
 * Created by Administrator on 06/09/13.
 */
public class Page4Fragment extends Fragment {

    private static final String TAG = "QParks";
    private static final String TAG_NAME = "nombre";
    private static final String TAG_WAIT = "tiempoEspera";
    private static final String TAG_STATUS = "estado";
    //private static final String TAG_OBJ_ATTR = "Attractions";
    private static final String TAG_ARR_ATTR = "results";
    private ListView lv;

    public View onCreateView(LayoutInflater inflater, final ViewGroup container,Bundle savedInstanceState) {
        // fragment not when container null
        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.page1,container,false);
        GetSOAPListener l = new GetSOAPListener(){

            @Override
            public void onRemoteCallComplete(Vector<SoapObject> soapFromNet) {
                ArrayList<Attrazione> attrList = new ArrayList<Attrazione>();
                for (SoapObject soap : soapFromNet) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    for (int k = 0; k < soap.getPropertyCount(); k++) {
                        SoapObject sobj = (SoapObject) soap.getProperty(k);
                        String key = sobj.getProperty(0).toString();
                        try {
                            String value = sobj.getProperty(1).toString();
                            map.put(key, value);
                        } catch (NullPointerException e) {
                            map.put(key, "nodata");
                            //e.printStackTrace();
                        }
                    }
                    Integer stato;
                    if (map.get(TAG_WAIT).equals("-1")) {
                        stato = 0;
                    }
                    else{
                        stato = Integer.parseInt(map.get(TAG_STATUS));
                    }

                    if (!map.get(TAG_WAIT).equals("nodata")) {
                        Attrazione mapAttr = new Attrazione(map.get(TAG_NAME), Integer.parseInt(map.get(TAG_WAIT)), stato);
                        attrList.add(mapAttr);
                    }
                }
                TextView t= null;
                if (view != null) {
                    t = (TextView)view.findViewById(R.id.textStatus);
                }
                t.setText("");
                Collections.sort(attrList);
                lv = (ListView)view.findViewById(R.id.listView_attr);
                ArrayAdapter arrAdapter = new AttrAdapter(container.getContext(), R.layout.list_item_p1,attrList);
                lv.setAdapter(arrAdapter);
            }
        };
        SOAPClient client = new SOAPClient(getActivity(), l, "PortAventura");
        client.execute();


        return view;

    }
}