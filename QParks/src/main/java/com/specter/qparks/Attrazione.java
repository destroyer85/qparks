package com.specter.qparks;

import java.util.Comparator;

/**
 * Created by Administrator on 08/09/13.
 */
public class Attrazione implements Comparable<Attrazione>{

    private String Nome;
    private Integer Tempo;
    private Integer Stato;

    public Attrazione(String nome, int tempo, int stato) {
        this.Nome = nome;
        this.Tempo = tempo;
        this.Stato = stato;
    }

    public String getNome() {
        return Nome;
    }

    public Integer getTempo() {
        return Tempo;
    }
    public Integer getStato() {
        return Stato;
    }

    public void setTempo(Integer tempo) {
        this.Tempo = tempo;
    }

    public int compareTo(Attrazione compareAttr) {

        int compareTempo = compareAttr.getTempo();

        //ascending order
        //return this.tempo - compareTempo;

        //descending order
        return compareTempo - this.Tempo;

    }

    public static Comparator<Attrazione> AttrNameComparator = new Comparator<Attrazione>() {

        public int compare(Attrazione attr1, Attrazione attr2) {

            String attrName1 = attr1.getNome().toUpperCase();
            String attrName2 = attr2.getNome().toUpperCase();

            //ascending order
            return attrName1.compareTo(attrName2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }
    };

}
