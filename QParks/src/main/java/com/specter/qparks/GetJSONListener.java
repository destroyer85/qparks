package com.specter.qparks;

/**
 * Created by Administrator on 05/09/13.
 */
import org.json.JSONObject;

public interface GetJSONListener {
    public void onRemoteCallComplete(JSONObject jsonFromNet);
}