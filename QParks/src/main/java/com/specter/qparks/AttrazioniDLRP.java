package com.specter.qparks;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.ZipInputStream;

/**
 * Created by Administrator on 17/09/13.
 */
public class AttrazioniDLRP extends AsyncTask<Void,Void,HashMap<String,String>> {
    public HashMap<String,String> AttrDLRPList = new HashMap<String,String>();
    private static final String TAG_ARR_ATTR = "l";

    @Override
    protected HashMap<String,String> doInBackground(Void... voids) {
        for (int i = 1; i <= 9; i++) {
	        String url = "http://disney.cms.pureagency.com/cms/ProxyContent?key=Ajjjsh%3BUj&json=%7B%22h1%22%3A0%2C%22s%22%3A0%2C%22lg%22%3A2%2C%22h2%22%3A0%2C%22tp%22%3A" + i + "%7D";
            HttpClient client = new DefaultHttpClient();
            HttpGet method = new HttpGet(url);
            try {
                HttpEntity httpEntity =client.execute(method).getEntity();
                InputStream is = httpEntity.getContent();
                ZipInputStream zis = new ZipInputStream(is);
                String strUnzipped = null;
                while (zis.getNextEntry() != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] bytes = new byte[2048];
                    int count;
                    while ((count = zis.read(bytes)) != -1) {
                        baos.write(bytes, 0, count);
                    }
                    strUnzipped= baos.toString("UTF-8");
                }
                zis.close();
                JSONObject json_tmp = new JSONObject(strUnzipped);
                JSONArray arr_attractions = json_tmp.getJSONArray(TAG_ARR_ATTR);
                for(int n = 0; n < arr_attractions.length(); n++){
                    JSONArray arr_attrazione = arr_attractions.getJSONArray(n);
                    String codice = arr_attrazione.getString(12);
                    String nome = arr_attrazione.getString(1);
                    AttrDLRPList.put(codice,nome);
                }
            }
            catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    return AttrDLRPList;
    }

}
