package com.specter.qparks;

/**
 * Created by Administrator on 08/09/13.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class AttrAdapter extends ArrayAdapter<Attrazione> {

    int resource;
    //Initialize adapter
    public AttrAdapter(Context context, int resource, List<Attrazione> items) {
        super(context, resource, items);
        this.resource=resource;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LinearLayout attrView;
        //Get the current alert object
        Attrazione al = getItem(position);

        //Inflate the view
        if(convertView==null)
        {
            attrView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater)getContext().getSystemService(inflater);
            vi.inflate(resource, attrView, true);
        }
        else
        {
            attrView = (LinearLayout) convertView;
        }
        //Get the text boxes from the listitem.xml file
        TextView attrName =(TextView)attrView.findViewById(R.id.name);
        TextView attrWait =(TextView)attrView.findViewById(R.id.wait);

        //Assign the appropriate data from our alert object above
        attrName.setText(al.getNome());
        switch(al.getStato()) {
            case 0:
                attrWait.setText(R.string.closed);
                break;
            case 2:
                attrWait.setText(R.string.interrupted);
                break;
            default:
                attrWait.setText(al.getTempo().toString());
        }

        return attrView;
    }

}