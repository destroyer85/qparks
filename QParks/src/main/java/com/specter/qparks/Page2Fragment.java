package com.specter.qparks;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Administrator on 06/09/13.
 */
public class Page2Fragment extends Fragment {

    private static final String TAG = "QParks";
    private static final String TAG_NAME = "code";
    private static final String TAG_WAIT = "time";
    private static final String TAG_STATUS = "delta";
    //private static final String TAG_OBJ_ATTR = "Attractions";
    private static final String TAG_ARR_ATTR = "results";
    private ListView lv;
    private AttrazioniEP attrEPList = new AttrazioniEP();

    public View onCreateView(LayoutInflater inflater, final ViewGroup container,Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.page1,container,false);
        GetJSONListener l = new GetJSONListener(){

            @Override
            public void onRemoteCallComplete(JSONObject jsonFromNet) {
                JSONArray arr_attractions;
                ArrayList<Attrazione> attrList = new ArrayList<Attrazione>();
                try{
                    arr_attractions = jsonFromNet.getJSONArray(TAG_ARR_ATTR);
                    for(int i = 0; i < arr_attractions.length(); i++){
                        JSONObject a = arr_attractions.getJSONObject(i);
                        int code = a.getInt(TAG_NAME);
                        int wait;
                        Integer stato=1;
                        try{
                            wait = a.getInt(TAG_WAIT);
                        }
                        catch (JSONException ejs){
                            wait = -1;
                            stato = 0;
                        }
                        String name = attrEPList.getNome(code);
                        Log.i(TAG, "Name=" + name);
                        Log.i(TAG, "DisplayWaitTime=" + wait);
                        Attrazione map = new Attrazione(name,wait,stato);
                        attrList.add(map);
                    }
                    TextView t= null;
                    if (view != null) {
                        t = (TextView)view.findViewById(R.id.textStatus);
                    }
                    try{
                        t.setText(jsonFromNet.getString(TAG_STATUS));
                    }
                    catch (JSONException ejs){
                        t.setText("No valid data!");
                    }
                    Collections.sort(attrList);
                    lv = (ListView)view.findViewById(R.id.listView_attr);
                    ArrayAdapter arrAdapter = new AttrAdapter(container.getContext(), R.layout.list_item_p1,attrList);
                    lv.setAdapter(arrAdapter);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        JSONClient client = new JSONClient(getActivity(), l,"EuropaPark");
        Long tempo = (System.currentTimeMillis()/1000) + 3600;
        String str1 = new SimpleDateFormat("yyyyMMddHHmm", Locale.GERMANY).format(new Date(System.currentTimeMillis()));
        String str2 = md5("Europa-Park" + str1 + "Webservice");
        String url = "http://apps.europapark.de/webservices/waittimes/index.php?code="+ str2 + "&v=2&base=" + tempo.toString();
        client.execute(url);
        Log.i(TAG, "str1=" + str1);
        Log.i(TAG, "str2=" + str2);
        Log.i(TAG, "tempo=" + tempo.toString());
        Log.i(TAG, "url=" + url);

        return view;
    }
    public String md5(String s) {
        try
        {
            MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
            localMessageDigest.reset();
            localMessageDigest.update(s.getBytes());
            byte[] arrayOfByte = localMessageDigest.digest();
            int i = arrayOfByte.length;
            StringBuilder localStringBuilder = new StringBuilder(i << 1);
            for (int j = 0; ; j++)
            {
                if (j >= i)
                {
                    //this.callback.success(localStringBuilder.toString());
                    return localStringBuilder.toString();
                }
                localStringBuilder.append(Character.forDigit((0xF0 & arrayOfByte[j]) >> 4, 16));
                localStringBuilder.append(Character.forDigit(0xF & arrayOfByte[j], 16));
            }
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();

        }
        return "";
    }

}