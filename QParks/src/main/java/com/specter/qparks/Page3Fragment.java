package com.specter.qparks;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 06/09/13.
 */
public class Page3Fragment extends Fragment {

    private static final String TAG = "QParks";
    private static final String TAG_NAME = "code";
    private static final String TAG_WAIT = "time";
    private static final String TAG_STATUS = "delta";
    //private static final String TAG_OBJ_ATTR = "Attractions";
    private static final String TAG_ARR_ATTR = "l";
    private ListView lv;
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.page1, container, false);
        final AttrazioniDLRP AttrDLRPList =new AttrazioniDLRP();
        AttrDLRPList.execute();

        GetHTTPListener l = new GetHTTPListener(){

            @Override
            public void onRemoteCallComplete(String jsonFromNet) {
                JSONObject json= null;
                try {
                    json = new JSONObject(jsonFromNet);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray arr_attractions;
                ArrayList<Attrazione> attrList = new ArrayList<Attrazione>();
                try{
                    arr_attractions = json.getJSONArray(TAG_ARR_ATTR);
                    for(int i = 0; i < arr_attractions.length(); i=i+5){
                        String codice = arr_attractions.getString(i);
                        //String oraApertura = arr_attractions.getString(i+1);
                        //String oraChiusura = arr_attractions.getString(i+2);
                        Integer stato = arr_attractions.getInt(i+3);
                        Integer wait = arr_attractions.getInt(i+4);
                        String name;
                        name = AttrDLRPList.AttrDLRPList.get(codice);
                        if(name!=null){
                            Log.i(TAG, "Name=" + name);
                            Log.i(TAG, "DisplayWaitTime=" + wait);
                            Attrazione map = new Attrazione(name,wait,stato);
                            attrList.add(map);
                        }
                    }
                    TextView t= null;
                    if (view != null) {
                        t = (TextView)view.findViewById(R.id.textStatus);
                    }
                    t.setText("");
                    Collections.sort(attrList);
                    lv = (ListView)view.findViewById(R.id.listView_attr);
                    ArrayAdapter arrAdapter = new AttrAdapter(container.getContext(), R.layout.list_item_p1,attrList);
                    lv.setAdapter(arrAdapter);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("key", "Ajjjsh;Uj"));
        HTTPClient client = new HTTPClient(getActivity(), l,urlParameters, "Disneyland");
        String url = "http://disney.cms.pureagency.com/cms/ProxyTempsAttente";
        client.execute(url);
        return view;
    }

}