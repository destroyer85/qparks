package com.specter.qparks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.KeepAliveHttpsTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import java.util.Vector;

/**
 * Created by Administrator on 29/09/13.
 */
public class SOAPClient extends AsyncTask<Void, Void, Vector<SoapObject>> {
    ProgressDialog progressDialog ;
    GetSOAPListener getSOAPListener;
    private static String METHOD_NAME_L = "login";
    private static String SOAP_ACTION_L = "urn:WebserviceControllerwsdl#login";
    private static String METHOD_NAME_U = "getUbicaciones";
    private static String SOAP_ACTION_U = "urn:WebserviceControllerwsdl#getUbicaciones";
    private static final String NAMESPACE = "urn:WebserviceControllerwsdl";
    private static final String HOST = "appmobile.portaventura.es";
    private static final Integer PORT = 443;
    private static final String URL = "/webserviceAuthenticate/scheme/ws/1";
    private static final Integer TIMEOUT= 30000;
    private String Parco;


    Context curContext;
    public SOAPClient(Context context, GetSOAPListener listener, String parco){
        this.getSOAPListener = listener;
        curContext = context;
        Parco = parco;
    }

    public static Vector<SoapObject> connect() throws SoapFault {
        String sessionCookie= null;


        SoapObject request= new SoapObject(NAMESPACE, METHOD_NAME_L);
        request.addProperty("user","android");
        request.addProperty("password","4NMixDgyT8aed6CaYp6Cvtsb5w1GVg7GSEUL1RVZ.");
        request.addProperty("accion","accion");
        request.addProperty("token","APA91bG_ji0TTzxdEVz0Vvn8tSbEbwavk-nTfOcJlvXwJXEcXJyzIO8qqUIf76TymtcJmlz4l1zOxlci-VYLeAPZU8BWIBQpsA5fJ79ZttI0uV-ds9hlGq6E0XA9WnSK5Lv3mZQbGfaIYDj51U6dndzNyggL9ZMxJ0oPq-e3USAw7Q4yyMSVY6A");
        request.addProperty("version","1.8.30");

        SoapSerializationEnvelope envelope=new SoapSerializationEnvelope (SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        KeepAliveHttpsTransportSE androidHttpsTransport=new KeepAliveHttpsTransportSE(HOST,PORT,URL,TIMEOUT);
        try{
            androidHttpsTransport.debug = true;
            androidHttpsTransport.call(SOAP_ACTION_L, envelope);
            sessionCookie=envelope.getResponse().toString();
            Log.i("WebService output", sessionCookie);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        request= new SoapObject(NAMESPACE, METHOD_NAME_U);
        request.addProperty("tipo","Atraccion");
        request.addProperty("zona","");
        request.addProperty("idioma","en");

        envelope=new SoapSerializationEnvelope (SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.bodyOut=request;

        Element[] header = new Element[1];
        header[0] = new Element().createElement(null, "cookie");
        header[0].addChild(Node.TEXT,sessionCookie);
        envelope.headerOut=header;


        try{
            androidHttpsTransport.call(SOAP_ACTION_U, envelope);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return (Vector<SoapObject>)envelope.getResponse();
    }

    @Override
    public void onPreExecute() {
        progressDialog = new ProgressDialog(curContext);
        progressDialog.setMessage("Loading " + Parco + " data. Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

    }

    @Override
    protected Vector<SoapObject> doInBackground(Void... arg0) {
        try {
            return connect();
        } catch (SoapFault soapFault) {
            soapFault.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Vector<SoapObject> soap) {
        getSOAPListener.onRemoteCallComplete(soap);
        progressDialog.dismiss();
    }
}
