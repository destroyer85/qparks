package com.specter.qparks;

import org.ksoap2.serialization.SoapObject;

import java.util.Vector;

/**
 * Created by Administrator on 29/09/13.
 */
public interface GetSOAPListener {
    public void onRemoteCallComplete(Vector<SoapObject> soapFromNet);
}
