package com.specter.qparks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Administrator on 15/09/13.
 */
public class HTTPClient extends AsyncTask<String, Void, String> {
    ProgressDialog progressDialog ;
    GetHTTPListener getHTTPListener;
    Context curContext;
    String Parco;
    private static List<NameValuePair> URLParameters;
    private static final String TAG = "QParks";

    public HTTPClient(Context context,GetHTTPListener listener,List<NameValuePair> urlParameters, String parco){
        this.getHTTPListener = listener;
        curContext = context;
        URLParameters = urlParameters;
        Parco = parco;
    }
    public static String connect(String url) {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", "Mio");

        try {
            post.setEntity(new UrlEncodedFormEntity(URLParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String strUnzipped = "capra";
        try {
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();
            ZipInputStream zis = new ZipInputStream(is);
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Log.i(TAG, "Unzipping " + ze.getName());
                byte[] bytes = new byte[2048];
                int count;
                while ((count = zis.read(bytes)) != -1) {
                    baos.write(bytes, 0, count);
                }
                strUnzipped= baos.toString("UTF-8");
                Log.i(TAG, "Contenuto " + strUnzipped);
            }
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strUnzipped;
    }
    public HashMap<String,String> AttrDLRPList = new HashMap<String,String>();
    private void recuperaATTRDLRP() {
        for (int i = 1; i <= 9; i++) {
            String url = "http://disney.cms.pureagency.com/cms/ProxyContent?key=Ajjjsh%3BUj&json=%7B%22h1%22%3A0%2C%22s%22%3A0%2C%22lg%22%3A2%2C%22h2%22%3A0%2C%22tp%22%3A" + i + "%7D";
            HttpClient client = new DefaultHttpClient();
            HttpGet method = new HttpGet(url);
            try {
                HttpEntity httpEntity =client.execute(method).getEntity();
                InputStream is = httpEntity.getContent();
                ZipInputStream zis = new ZipInputStream(is);
                String strUnzipped = null;
                while (zis.getNextEntry() != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] bytes = new byte[2048];
                    int count;
                    while ((count = zis.read(bytes)) != -1) {
                        baos.write(bytes, 0, count);
                    }
                    strUnzipped= baos.toString("UTF-8");
                }
                zis.close();
                JSONObject json_tmp = new JSONObject(strUnzipped);
                JSONArray arr_attractions = json_tmp.getJSONArray("l");
                for(int n = 0; n < arr_attractions.length(); n++){
                    JSONArray arr_attrazione = arr_attractions.getJSONArray(n);
                    String codice = arr_attrazione.getString(12);
                    String nome = arr_attrazione.getString(1);
                    AttrDLRPList.put(codice,nome);
                }
            }
            catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected String doInBackground(String... urls) {
        return connect(urls[0]);
    }
    @Override
    public void onPreExecute() {
        progressDialog = new ProgressDialog(curContext);
        progressDialog.setMessage("Loading " + Parco + " data. Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String strUnzipped) {
        getHTTPListener.onRemoteCallComplete(strUnzipped);
        progressDialog.dismiss();
    }
    public String getATTRDLRPName(String codice){
        return AttrDLRPList.get(codice);
    }
}
