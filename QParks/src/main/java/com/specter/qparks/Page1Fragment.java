package com.specter.qparks;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Administrator on 06/09/13.
 */
public class Page1Fragment extends Fragment {

    private static final String TAG = "QParks";
    // JSON Node names
    private static final String TAG_NAME = "Name";
    private static final String TAG_WAIT = "DisplayWaitTime";
    private static final String TAG_STATUS = "Status";
    private static final String TAG_OBJ_ATTR = "Attractions";
    private static final String TAG_ARR_ATTR = "Attraction";
    private ListView lv;

    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,Bundle savedInstanceState) {

        // fragment not when container null
        if (container == null) {
            return null;
        }
        final View view = inflater.inflate(R.layout.page1,container,false);
        GetJSONListener l = new GetJSONListener(){

            @Override
            public void onRemoteCallComplete(JSONObject jsonFromNet) {
                JSONObject attractions;
                JSONArray arr_attractions;
                ArrayList<Attrazione> attrList = new ArrayList<Attrazione>();
                try{
                    attractions = jsonFromNet.getJSONObject(TAG_OBJ_ATTR);
                    arr_attractions = attractions.getJSONArray(TAG_ARR_ATTR);
                    for(int i = 0; i < arr_attractions.length(); i++){
                        JSONObject a = arr_attractions.getJSONObject(i);
                        if(!a.getString(TAG_NAME).equals("(null)")){
                            String name = a.getString(TAG_NAME);
                            Integer wait;
                            Integer stato;
                            if (a.getString(TAG_WAIT).equals(" (null)")) {
                                wait = -1;
                                stato = 2;
                            }
                            else{
                                wait = a.getInt(TAG_WAIT);
                                stato = 1;
                            }
                            Log.i(TAG, "Name=" + name);
                            Log.i(TAG, "DisplayWaitTime=" + wait);
                            Attrazione map = new Attrazione(name,wait,stato);
                            attrList.add(map);
                        }
                    }
                    TextView t= null;
                    if (view != null) {
                        t = (TextView)view.findViewById(R.id.textStatus);
                    }
                    t.setText(jsonFromNet.getString(TAG_STATUS));
                    Collections.sort(attrList);
                    lv = (ListView)view.findViewById(R.id.listView_attr);
                    ArrayAdapter arrAdapter = new AttrAdapter(container.getContext(), R.layout.list_item_p1,attrList);
                    lv.setAdapter(arrAdapter);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        JSONClient client = new JSONClient(getActivity(), l, "Gardaland");
        String url = "http://merlincms.com/qfetcher/GardaQ.json";
        client.execute(url);


        return view;
    }

}