package com.specter.qparks;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import java.util.List;


/**
 * Created by Administrator on 06/09/13.
 */
public class PagerAdapter extends FragmentPagerAdapter {

    // fragments to instantiate in the viewpager
    private List<Fragment> fragments;
    private String[] fragmentsTitle;

    // constructor
    public PagerAdapter(FragmentManager fm,List<Fragment> fragments, String[] fragmentsTitle) {
        super(fm);
        this.fragments = fragments;
        this.fragmentsTitle = fragmentsTitle;
    }

    // return access to fragment from position, required override
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    // number of fragments in list, required override
    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public String getPageTitle(int position) {
        return fragmentsTitle[position];
    }

}
